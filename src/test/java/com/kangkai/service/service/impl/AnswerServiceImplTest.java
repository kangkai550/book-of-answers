package com.kangkai.service.service.impl;

import com.kangkai.entity.Answer;
import com.kangkai.service.AnswerService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by kangkai on 2017/3/26.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AnswerServiceImplTest {

    @Resource
    private AnswerService answerService;

    @Test
    public void listAnswer() throws Exception {
        Assert.assertNotNull( answerService.listAnswer());
    }

    @Test
    public void getAnswerById() throws Exception {
        Assert.assertNotNull( answerService.getAnswerById(4));

    }

    @Test
    public void addAnswer() throws Exception {
        Answer answer = new Answer();
        answer.setKeyword("Test" + System.currentTimeMillis());
        Assert.assertNotNull( answerService.addAnswer(answer));
    }

    @Test
    public void updateAnswer() throws Exception {
        Answer answer = new Answer();
        answer.setKeyword("Test 111");
        answer.setId(2);
        answerService.updateAnswer(answer);
        Assert.assertEquals("Test 111",answerService.getAnswerById(answer.getId()).getKeyword());
    }

    @Test
    public void deleteAnswer() throws Exception {
        Answer answer = new Answer();
        answer.setKeyword("Test" + System.currentTimeMillis());
        answer = answerService.addAnswer(answer);
        answerService.deleteAnswer(answer.getId());
        Assert.assertNull(answerService.getAnswerById(answer.getId()));
    }

    @Test
    public void findAnswerByKeyword() throws Exception {
        Assert.assertNotNull( answerService.findAnswerByKeyword("Test"));
    }

    @Test
    public void findAnswerByOwner() throws Exception {
        Assert.assertNotNull( answerService.findAnswerByOwner("The Book of Answers"));
    }

}