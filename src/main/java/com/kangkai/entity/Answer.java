package com.kangkai.entity;

import com.sun.xml.internal.ws.api.model.MEP;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 答案实体
 *
 * Created by kangkai on 2017/3/26.
 */
@ApiModel
@Entity(name = "T_BOA_ANSWER")
public class Answer {

    /**
     * 这里设置 ID 为自增的 Integer
     */
    @ApiModelProperty("答案 ID")
    @Id
    @GeneratedValue
    @Column(name = "FID")
    private Integer id;

    /**
     * 答案内容
     */
    @ApiModelProperty("答案内容")
    @Size(min = 1,max = 32, message = "自定义答案太长")
    @NotEmpty(message = "自定义答案不可以为空")
    @Column(name = "FKEY_WORD")
    private String keyword;

    /**
     * 答案获取次数
     */
    @ApiModelProperty("答案获取次数")
    @Column(name = "FCOUNT", columnDefinition = "INT DEFAULT 0")
    private Integer count;

    /**
     * 答案持有者，也就是上传答案的人
     */
    @ApiModelProperty("答案持有者，也就是上传答案的人")
    @Size(min = 1, max = 32, message = "拥有者名称太长")
    @Column(name = "FOWNER", columnDefinition = "VARCHAR(32) NOT NULL DEFAULT 'The Book of Answers'")
    private String owner;

    /**
     * 答案创建时间
     */
    @ApiModelProperty("答案创建的时间")
    @Column(name = "FCREATE_TIME", columnDefinition="timestamp default current_timestamp")
    private Date createTime;

    public Answer() {
        //使用hibernate 必须要有一个无参的构造方法，不然数据库操作会报错
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
