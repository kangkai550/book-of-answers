package com.kangkai.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by kangkai on 2017/3/26.
 */

/**
 * 这个注解可以把 yml 的属性映射过来并封装到一个实体中，这里没什么用，只是试用一下这个功能
 */
@Component
@ConfigurationProperties(prefix = "answer")
public class AnswerProperties {
    /**
     * 答案文字
     */
    private String word;

    /**
     * 答案获取次数
     */
    private int count;

    /**
     * 答案等级
     */
    private String level;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "AnswerProperties{" +
                "word='" + word + '\'' +
                ", count=" + count +
                ", level='" + level + '\'' +
                '}';
    }
}
