package com.kangkai.controller;

import com.kangkai.properties.AnswerProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by kangkai on 2017/3/26.
 */
@RestController
@Api(description = "这里添加业务描述")
public class HelloController {

    @Resource
    private AnswerProperties answerProperties;



    @GetMapping(value = {"/hello"})
    @ApiOperation(value = "这里添加接口描述", notes = "这里可以添加接口业务逻辑描述")
    public String hello(){
        return answerProperties.toString();
    }
}

