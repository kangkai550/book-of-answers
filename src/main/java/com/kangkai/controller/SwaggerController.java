package com.kangkai.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Swagger 专用跳转 controller
 * Created by kangkai on 2017/4/1.
 */
@Controller
@ApiIgnore
public class SwaggerController {
    @GetMapping(value = "/swagger")
    public String index() {
        return "redirect:swagger-ui.html";
    }
}
