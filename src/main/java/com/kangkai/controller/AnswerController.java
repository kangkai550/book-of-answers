package com.kangkai.controller;

import com.kangkai.common.Result;
import com.kangkai.entity.Answer;
import com.kangkai.service.AnswerService;
import com.kangkai.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 答案-控制器
 * Created by kangkai on 2017/3/26.
 */
@RestController
@RequestMapping("/boa")
@Api(description = "答案控制器，处理和答案有关的请求")
public class AnswerController {

    @Resource
    private AnswerService answerService;


    /**
     * 查询答案列表
     * @return
     */
    @ApiOperation(value = "查询答案列表", notes = "获取答案的列表信息，先从缓存获取，缓存未命中则从业务层获取")
    @GetMapping(value = "/answers")
    public Result<Answer> answerList(){

        return ResultUtil.success(answerService.listAnswer());
    }

    /**
     * 添加一个答案
     * @param answer
     * @param bindingResult
     * @return
     */
    @ApiOperation(value = "添加一个答案", notes = "可以自行添加答案信息",consumes = "application/form-data")
    @PostMapping(value = "/answers")
    public Result<Answer> answerAdd(@Valid Answer answer, BindingResult bindingResult) throws Exception {
        return ResultUtil.success(answerService.addAnswer(answer));
    }

    /**
     * 根据 id 查询一个答案
     * @param id
     * @return
     */
    @ApiOperation(value = "根据 ID 查询答案信息", notes = "根据 id 查询答案信息")
    @GetMapping(value = "/answers/{id}")
    public Result<Answer> answerFindOne(@PathVariable("id") Integer id){
        return ResultUtil.success(answerService.getAnswerById(id));

    }


    /**
     * 更新一个答案的信息
     * @param id
     * @param keyWord
     * @param count
     * @return
     */
    @ApiOperation(value = "根据 id 更新答案信息")
    @PutMapping(value = "/answers/{id}")
    public Result<Answer> answerUpdate(@PathVariable("id") Integer id,@RequestParam("keyWord") String keyWord,@RequestParam("count") Integer count){
        Answer answer = new Answer();
        answer.setKeyword(keyWord);
        answer.setId(id);
        answer.setCount(count);

        return ResultUtil.success(answerService.updateAnswer(answer));
    }

    /**
     * 删除一个答案
     * @param id
     */
    @ApiOperation(value = "根据 id 删除一个答案")
    @DeleteMapping(value = "/answers/{id}")
    public Result<Answer> answerDelete(@PathVariable("id") Integer id){
        answerService.deleteAnswer(id);
       return ResultUtil.success();
    }

    @ApiOperation(value = "根据关键字获取答案",notes = "这里只支持完全匹配")
    @GetMapping(value = "/answers/keyword/{keyword}")
    public Result<Answer> answerListByKeyword(@PathVariable("keyword") String keyword){
        return ResultUtil.success(answerService.findAnswerByKeyword(keyword));
    }

    @ApiOperation(value = "根据答案持有者获取答案",notes = "这里只支持完全匹配")
    @GetMapping(value = "/answers/owner/{owner}")
    public Result<Answer> answerListByOwner(@PathVariable("owner") String owner){
        return ResultUtil.success(answerService.findAnswerByOwner(owner));
    }
}

