package com.kangkai.repository;

import com.kangkai.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 这里可以自定义数据库操作的方法，方法名有讲究
 *
 * findBy+实体的字段名字，可以按照字段查询
 * Created by kangkai on 2017/3/26.
 */
@Repository
public interface AnswerRepository extends JpaRepository<Answer,Integer> {

    /**
     * 通过答案的内容来查询一个答案的实体，内容完全匹配，非模糊查找
     * @param keyword
     * @return
     */
    List<Answer> findByKeyword(String keyword);

    /**
     * 通过拥有者名字获取答案列表，内容完全匹配，非模糊查找
     * @param owner
     * @return
     */
    List<Answer> findByOwner(String owner);
}
