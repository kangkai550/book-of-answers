package com.kangkai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot 入口
 * Created by kangkai on 2017/3/26.
 */
@SpringBootApplication
public class BookOfAnswersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookOfAnswersApplication.class, args);
	}
}
