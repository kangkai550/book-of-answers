package com.kangkai.common;

/**
 * 错误码枚举类
 *
 * 把错误码和错误信息定义在同一个地方，方便维护，降低耦合
 *
 * Created by kangkai on 2017/3/26.
 */
public enum ErrorCodeEnum {
    SUCCESS(200,"success"),
    UNKNOWN_ERROR(-1,"未知错误"),
    PARAM_VALID_ERROR(900000,"参数校验失败,参数不合法"),
    ANSWER_INSERT_ERROR(900001,"答案插入失败,答案已存在"),
    ANSWER_UPDATE_ERROR(900002,"答案更新失败,答案 id 不存在"),
    ANSWER_DELETE_ERROR(900003,"答案删除失败,答案 id 不存在");

    private Integer code;

    private String msg;

    ErrorCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
