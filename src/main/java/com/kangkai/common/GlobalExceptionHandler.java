package com.kangkai.common;

import com.kangkai.exception.GlobalException;
import com.kangkai.utils.ResultUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 *  全局异常拦截类
 *
 * Created by kangkai on 2017/3/26.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e){
        if(e instanceof GlobalException){
            //这里捕获全局自定义异常
            GlobalException globalException = (GlobalException)e;
            return ResultUtil.error(globalException.getCode(),globalException.getMessage());
        }else if(e instanceof ConstraintViolationException){
            //这里捕获实体校验的相关异常
            ConstraintViolationException constraintViolationException = (ConstraintViolationException)e;
            List<ConstraintViolation> errorParamlist = new ArrayList(constraintViolationException.getConstraintViolations());
            return ResultUtil.error( ErrorCodeEnum.PARAM_VALID_ERROR.getCode(),errorParamlist.get(0).getMessage());
        }else{
            return ResultUtil.error(-1,e.getMessage());
        }

    }
}
