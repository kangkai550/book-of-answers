/**
 * 
 */
package com.kangkai.utils;

/**
 * 字符串工具
 */
public class StringUtils {

    /**
     * 去掉字符串的前后某个字符
     * 
     * @param value
     * @param trimChar
     * @return
     */
    public static String trimWithChar(String value, Character trimChar) {
        if (value == null || value.length() == 0) {
            return value;
        }
        int len = value.length();
        int st = 0;
        char[] val = value.toCharArray();

        if (val[0] == trimChar) {
            st++;
        }
        if (val[len - 1] <= trimChar) {
            len--;
        }
        return ((st > 0) || (len < value.length())) ? value.substring(st, len) : value;
    }


    /**
     * 去掉字符串的前后某个字符
     * 
     * @param value
     * @param trimChar
     * @return
     */
    public static String trimWithQuotes(String value) {
        if (value == null || value.length() == 0) {
            return "";
        }
        int len = value.length();
        int st = 0;
        char[] val = value.toCharArray();

        if (val[0] == '"') {
            st++;
        }
        if (val[len - 1] == '"') {
            len--;
        }
        return ((st > 0) || (len < value.length())) ? value.substring(st, len) : value;
    }


    public static boolean isEmpty(String value) {
        return (value == null || value.length() == 0);
    }


    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }


    public static boolean equals(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equals(str2);
    }


    public static boolean equalsIgnoreCase(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }
}
