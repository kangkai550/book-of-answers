package com.kangkai.utils;

import com.kangkai.common.Result;

/**
 * 全局返回结果工具类
 *
 * Created by kangkai on 2017/3/26.
 */
public class ResultUtil {

    /**
     * 请求成功，有数据返回的 Result
     * @param object
     * @return
     */
    public static Result success(Object object){
        Result result = new Result();
        result.setCode(200);
        result.setMsg("success");
        result.setData(object);
        return result;
    }

    /**
     * 请求成功，无数据返回的 Result
     * @return
     */
    public static Result success(){
        return success(null);
    }

    /**
     * 请求失败，有数据返回的 Result
     * @param code
     * @param msg
     * @return
     */
    public static Result error(Integer code, String msg){
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
