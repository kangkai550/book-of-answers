package com.kangkai.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * http 请求记录切面
 * Created by kangkai on 2017/3/26.
 */
@Aspect
@Component
public class HttpAspect {

    private final static Logger LOGGER = LoggerFactory.getLogger(HttpAspect.class);

    @Resource
    private ApplicationContext context;


    /**
     * 更优雅的写法
     * 这里面要指定到某个 Controller，不能指定到包，否则会报错
     */
    @Pointcut("execution(public * com.kangkai.controller.AnswerController.*(..))")
    public void log(){
        LOGGER.info("我想知道 log 方法在什么时候执行了");
        LOGGER.info("事实证明，这个方法体不会执行，只是一个切入点的标记");
    }

    /**
     * 这个方法在执行具体方法前执行
     */
    @Before("log()")
    public void doBefore(JoinPoint joinPoint){
        //这段代码获取请求的相关参数
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //URL
        LOGGER.info("url={}",request.getRequestURI());
        //请求的方式
        LOGGER.info("method={}",request.getMethod());
        //客户端 IP
        LOGGER.info("ip={}",request.getRemoteAddr());
        //请求了哪个类方法
        LOGGER.info("class_method={}",joinPoint.getSignature().getDeclaringTypeName() + "_" + joinPoint.getSignature().getName());
        //请求的参数
        LOGGER.info("args={}",joinPoint.getArgs());

        //这里可以把请求信息入库
    }

    /**
     * 这个方法在执行具体方法后执行
     */
    @After("log()")
    public void doAfter(){
        LOGGER.info("doAfter 执行了");
    }

    @AfterReturning(returning = "object",pointcut = "log()")
    public void doAfterReturning(Object object){
//        LOGGER.info("response={}",object.toString());
        //这里可以用于记录异常
    }

    /**
     * 比较 low 的写法
     */
//    /**
//     * 这个方法在执行具体方法前执行
//     * 这里面要指定到某个 Controller，不能指定到包，否则会报错
//     */
//    @Before("execution(public * com.kangkai.controller.AnswerController.*(..))")
//    public void doBefore(){
//        System.out.println("1111");
//    }
//
//    /**
//     * 这个方法在执行具体方法后执行
//     * 这里面要指定到某个 Controller，不能指定到包，否则会报错
//     * 重构之后可以提取出一个 Pointcut
//     */
//    @After("execution(public * com.kangkai.controller.AnswerController.*(..))")
//    public void doAfter(){
//        System.out.println(222222);
//    }
}
