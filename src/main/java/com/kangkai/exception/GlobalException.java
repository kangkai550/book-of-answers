package com.kangkai.exception;

import com.kangkai.common.ErrorCodeEnum;

/**
 *  全局异常封装类
 *
 *  这里注意要继承 RuntimeException
 *
 *  因为 spring 只对抛出的 RuntimeException 进行事务回滚
 *
 * Created by kangkai on 2017/3/26.
 */
public class GlobalException extends RuntimeException{

    private Integer code;

    public GlobalException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum.getMsg());
        this.code = errorCodeEnum.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
