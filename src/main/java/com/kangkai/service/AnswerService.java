package com.kangkai.service;

import com.kangkai.entity.Answer;

import java.util.List;

/**
 * Created by kangkai on 2017/3/26.
 */
public interface AnswerService {
    /**
     * 获取答案列表
     */
    List<Answer> listAnswer();

    /**
     * 获取单个答案信息
     */
    Answer getAnswerById(Integer id);

    /**
     * 添加单个答案
     */
    Answer addAnswer(Answer answer) throws Exception;

    /**
     * 更新一个答案
     */
    Answer updateAnswer(Answer answer);

    /**
     * 删除一个答案
     */
    void deleteAnswer(Integer id);

    /**
     * 根据答案内容获取答案列表
     * @param keyWord
     * @return
     */
    List<Answer> findAnswerByKeyword(String keyWord);

    /**
     * 查询该答案拥有者的所有答案
     * @param owner
     * @return
     */
    List<Answer> findAnswerByOwner(String owner);
}
