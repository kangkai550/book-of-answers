package com.kangkai.service.service.impl;

import com.kangkai.common.ErrorCodeEnum;
import com.kangkai.entity.Answer;
import com.kangkai.exception.GlobalException;
import com.kangkai.repository.AnswerRepository;
import com.kangkai.service.AnswerService;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by kangkai on 2017/3/26.
 */
@Service
public class AnswerServiceImpl implements AnswerService{

    @Resource
    private AnswerRepository answerRepository;

    @Override
    public List<Answer> listAnswer() {
        return answerRepository.findAll();
    }

    @Override
    public Answer getAnswerById(Integer id) {
        return answerRepository.findOne(id);
    }

    @Override
    public Answer addAnswer(Answer answer) throws Exception {
        //判断答案内容是否存在，存在则不能更新
        if(answerRepository.findByKeyword(answer.getKeyword()).size() != 0){
            throw new GlobalException(ErrorCodeEnum.ANSWER_INSERT_ERROR);
        }else{
            //若答案的拥有者为空，则设置默认值
            if(StringUtils.isNullOrEmpty(answer.getOwner())){
                answer.setOwner("The Book of Answers");
            }
            //设置生成时间
            answer.setCreateTime(new Date());
            //设置初始次数
            answer.setCount(0);
            return answerRepository.save(answer);
        }

    }

    @Override
    public Answer updateAnswer(Answer answer) {
        //判断该实体是否存在，如果不存在，hibernate 会自动插入一条新数据，所以这里要进行判断
        if(!answerRepository.exists(answer.getId())){
            throw new GlobalException(ErrorCodeEnum.ANSWER_UPDATE_ERROR);
        }else{
            //把数据 load 出来，再修改，这里2次数据库操作了，比较浪
            Answer answerPersistance = answerRepository.findOne(answer.getId());
            //更新字段值
            if(!StringUtils.isNullOrEmpty(answer.getOwner())){
                answerPersistance.setOwner(answer.getOwner());
            }
            if(!StringUtils.isNullOrEmpty(answer.getKeyword())){
                answerPersistance.setKeyword(answer.getKeyword());
            }
            return answerRepository.save(answerPersistance);
        }
    }

    @Override
    public void deleteAnswer(Integer id) {
        //判断该实体是否存在
        if(!answerRepository.exists(id)){
            throw new GlobalException(ErrorCodeEnum.ANSWER_DELETE_ERROR);
        }else{
            answerRepository.delete(id);
        }
    }

    @Override
    public List<Answer> findAnswerByKeyword(String keyword) {
        return answerRepository.findByKeyword(keyword);
    }

    @Override
    public List<Answer> findAnswerByOwner(String owner) {
        return answerRepository.findByOwner(owner);
    }
}
